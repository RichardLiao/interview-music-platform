// component getYouTube
import axios from "axios";
function _getYouTube(method, data) {
  return fetch(
    `https://www.googleapis.com/youtube/v3/${method}?key=AIzaSyA0uc8XLsHpRmVg9aj1uzwW0646CBAORbA&${data}`
  )
    .then(function(resp) {
      return resp.json();
    })
    .then(function(data) {
      return data;
    });
}
// function _getKKBOXauth() {
//   return axios(
//     `https://account.kkbox.com/oauth2/token`,
//     {
//       headers: {
//         Authorization:
//           "W0000NZP6b3000000045900001SYI-001BuSjAbED0P135PoQDS001SdnBA2957"
//       }
//     }
//   )
  
// }

function _getKKBOXFollow(method) {
  return axios(
    `https://api-listen-with.kkbox.com.tw/v1/channels/13459493/broadcast-history`,
    {
      headers: {
        Authorization:
          "W0000NZP6b3000000045900001SYI-001BuSjAbED0P135PoQDS001SdnBA2957"
      }
    }
  ).then(function(res) {
    return res;
  });
}

function _getKKBOX(method, data) {
  return axios(`https://api.kkbox.com/v1.1${method}`, {
    headers: { Authorization: "Bearer 6qrupcvRlUFCjdBk6Emr-g==" },
    params: data
  }).then(function(res) {
    return res;
  });
}
function loadScript(url) {
  var script = document.createElement("script");
  var index = document.getElementsByTagName("script")[0];
  script.src = url;
  index.parentNode.insertBefore(script, index);
}
const serialize = function(obj) {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
};

function parseDate(date) {
  let a = Date.parse(date);
  // let year = new Date(a).getFullYear()
  // let month = new Date(a).getMonth()
  // let day = new Date(a).getDate()
  // let hour = new Date(a).getHours()
  // let min = new Date(a).getMinutes()
  var TimeNow = new Date(a);
  var year = TimeNow.toLocaleDateString().slice(0, 4);
  var month =
    (TimeNow.getMonth() + 1 < 10 ? "0" : "") + (TimeNow.getMonth() + 1);
  var day = (TimeNow.getDate() < 10 ? "0" : "") + TimeNow.getDate();
  var hour = (TimeNow.getHours() < 10 ? "0" : "") + TimeNow.getHours();
  var min = (TimeNow.getMinutes() < 10 ? "0" : "") + TimeNow.getMinutes();
  return `${year}/${month}/${day} ${hour}:${min}`;
}

function compareBy(key) {
  var re = /^[0-9]+.?[0-9]*/;
  return function(a, b) {
    if (!re.test(a[key]) || !re.test(b[key])) {
      console.log("我不是數字");
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
    } else {
      console.log("我是數字");
      if (parseInt(a[key]) < parseInt(b[key])) return -1;
      if (parseInt(a[key]) > parseInt(b[key])) return 1;
    }
  };
}

// 過濾相同的
const dedupeByProperty = (arr, objKey) => {
  return arr.reduce(
    (acc, curr) =>
      acc.some(a => a[objKey] === curr[objKey]) ? acc : [...acc, curr],
    []
  );
};

export {
  dedupeByProperty,
  compareBy,
  _getYouTube,
  _getKKBOX,
  _getKKBOXFollow,
  serialize,
  parseDate,
  loadScript
};
