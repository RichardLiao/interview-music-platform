import React, { Component } from "react";

import YouTubeIframe from "./youtubeIframe";
import YoutubePanel from "./youtubePanel";
import firebase from "firebase";
import Sidebar from "../../components/Sidebar";
import { AppContext } from "../../context/YouTubeProvider";
import {
  _getYouTube,
  _getKKBOX,
  _getKKBOXFollow,
  serialize
} from "../../utils/config";
// import { Radio } from "antd";

import "./css/index.scss";
import ModalSearch from "./modalsearch.js";
import PlayCategory from "./category.js";
import PlayList from "./playlist.js";
import { resolve } from "url";

// firebase
var config = {
  apiKey: "AIzaSyA8jlO3Nu3ilPboJcup8oULMyO_crolcgI",
  authDomain: "interview-musicbox.firebaseapp.com",
  databaseURL: "https://interview-musicbox.firebaseio.com",
  storageBucket: "interview-musicbox.appspot.com",
  projectId: "interview-musicbox"
  // storageBucket: "",
  // messagingSenderId: "141415165779"
};
var firebaseAPP = firebase.initializeApp(config);
const CoolRef = firebaseAPP.database().ref("cool");

// YouTubeIframe_Component

class YouTubeAPIBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optTypeCheck: "video", //video | live
      optTimeCheck: "month", // all | month
      optPopularCheck: "popular", // date | popular
      searchModal: false,
      isToggle: true,
      YouTubeID: "",
      colorTheme: "#17a2b8",
      isType: "video",
      keyword: "",
      searchList: [],
      datas: { items: [] },
      data_playlist: { items: [] },
      optionsOrder: [
        {
          value: "date",
          label: "date"
        },
        {
          value: "viewCount",
          label: "viewCount"
        }
      ],
      optionsOrderBind: "date",
      optionsTime: [
        {
          value: "1911-11-01T00:00:00Z",
          label: "所有時間"
        },
        {
          value: (() => {
            // 需回傳 RFC 3339 格式 "2017-11-01T00:00:00Z"
            const currentDate = new Date();
            currentDate.setDate(currentDate.getDate() - 30);
            const parseDate = currentDate.toJSON();
            // console.log(parseDate);
            return parseDate;
          })(),
          label: "一個月內"
        }
      ],
      optionsTimeBind: "2018-11-01T00:00:00Z", //需回傳 RFC 3339 格式
      playlist_link: "",
      singer_info: {
        "Red Velvet": [
          {
            name: "Irene",
            img: "http://www.bunnygo.net/images/20170221/20170221114418541.jpg",
            descript:
              " 裴柱現，藝名Irene，出生於韓國大邱市，韓國女歌手，為韓國SM娛樂旗下的五人女子組合Red Velvet的成員兼隊長。"
          }
        ]
      },
      nowPlay: {},
      isFavorite: "favorite", //favorite, followme, kkboxList, searchList
      whichCategorylist: "", //kkbox, hitfm
      whichPlay: "playall",
      favoriteData: [],
      kkboxFollow: [],
      kkboxFollowList: [],
      clickedCharts: "D_raAN9ydcm7WKkNlg",
      selectedA: ""
    };
    this.favoriteListWrap = React.createRef();
    this.YouTubeIframeRef = React.createRef();
    this.singerInfo = this.singerInfo.bind(this);
  }

  componentDidMount() {
    const { optionsTimeBind } = this.state;
    const data = {
      // 時間
      publishedAfter: optionsTimeBind,
      publishedBefore: new Date().toJSON(),
      q: this.state.keyword, //twice
      part: "snippet",
      maxResults: 30,
      // 排序
      order: this.state.optionsOrderBind //date,viewCount,rating,relevance,videoCount
    };
    // 預設為華語熱門單曲
    this.kkboxChart("D_raAN9ydcm7WKkNlg");
    this.charts();
    this.YouTube_search(data);

    // 讀取 firebase
    this._loadFirebase();
    this.kkboxFollowData();

    // 讀取搜尋清單
    if (!!localStorage.getItem("searchList")) {
      const searchList = JSON.parse(localStorage.getItem("searchList"));
      this.setState({ searchList });
    }
  }
  _loadFirebase() {
    CoolRef.on("value", snapshot => {
      const ary = [];
      snapshot.forEach(function(item) {
        ary.push(item.val());
      });
      let deepCopyFirebase = [...ary];
      deepCopyFirebase.reverse();
      this.setState({
        favoriteData: deepCopyFirebase
      });
    });
  }

  // YouTube API - playlists(播放清單)
  async YouTube_playlists(item) {
    const videoID = item.id;
    const data = {
      channelId: item.snippet.channelId,
      maxResults: 30,
      part: "snippet,contentDetails"
    };
    let res = await _getYouTube("playlists", serialize(data));
    res.items.map(listItem => {
      if (listItem.snippet.title === "TWICE") {
        const playlistID = listItem.id;
        this.setState({
          playlist_link: `https://www.youtube.com/watch?v=${videoID}&list=${playlistID}`
        });
      }
    });
  }
  async kkboxChart(id) {
    const data = {
      territory: "TW",
      offset: 0,
      limit: 30
    };
    let res = await _getKKBOX(`/charts/${id}/tracks`, data);
    this.setState({
      kkboxChartList: res.data.data
    });
  }
  async kkboxFollowData() {
    let res = await _getKKBOXFollow();
    this.setState({
      kkboxFollowList: res.data.data
    });
  }

  async charts() {
    const data = {
      territory: "TW"
    };
    let res = await _getKKBOX("/charts", data);
    this.setState({
      kkboxCharts: res.data.data
    });
  }
  // YouTube API - videos
  async YouTube_videos(datas) {
    const items = datas.items;
    const ids = [];
    items.map(item => {
      ids.push(item.id.videoId);
    });

    const data = {
      part: "snippet,contentDetails,statistics",
      id: ids.toString()
    };
    let res = await _getYouTube("videos", serialize(data));

    this.setState({
      datas: res
    });
  }

  // YouTube API - search
  async YouTube_search(data) {
    let res = await _getYouTube("search", serialize(data));
    this.YouTube_videos(res);
  }
  async readIds(kkboxChartList) {
    const reads = kkboxChartList.map(async (val, idx) => {
      const data = {
        q: `${val.album.artist.name} ${val.name}`,
        part: "snippet",
        maxResults: 30,
        // 排序
        order: "relevance" //date,viewCount,rating,relevance,videoCount
      };
      let res = await _getYouTube("search", serialize(data));
      const items = res.items;
      // console.log(items);
      return items && items[0].id.videoId;
    });
    const results = await Promise.all(reads);
    const arr = [];
    results.map(async v => {
      const data = {
        part: "snippet,contentDetails,statistics",
        id: v
      };
      let res = await _getYouTube("videos", serialize(data));
      arr.push(res.items);
      this.setState({
        favoriteData: arr.flat()
      });
    });
  }
  clickCharts = async (e, chartID) => {
    // 取得 kkbox chartlist 清單
    this.kkboxChart(chartID);
    const { kkboxChartList } = this.state;
    this.readIds(kkboxChartList);
    this.setState({
      isFavorite: "kkboxList",
      clickedCharts: chartID
    });
  };
  playkkboxList = () => {
    let ids = this.state.kkboxFollowList.map(async (val, idx) => {
      const data = {
        q: `${val.name} ${val.artist.name}`,
        part: "snippet",
        maxResults: 10,
        // 排序
        order: "relevance" //date,viewCount,rating,relevance,videoCount
      };
      let res = await _getYouTube("search", serialize(data));
      return res.items[0] && res.items[0].id.videoId;
    });
    Promise.all(ids).then(async results => {
      const data = {
        part: "snippet,contentDetails,statistics",
        id: results.toString()
      };
      let res = await _getYouTube("videos", serialize(data));
      this.setState(
        {
          isFavorite: "followme",
          clickedCharts: "followme"
        },
        () => {
          this.setState({
            favoriteData: res.items
          });
        }
      );
    });
  };
  clickKkboxFollowFn = () => {
    this.playkkboxList();
  };
  
  clickSearchList = () => {
    this.setState(
      {
        isFavorite: "searchList",
        clickedCharts: "",
        whichCategorylist: ""
      },
      () => {
        this.setState({
          favoriteData: JSON.parse(localStorage.getItem("searchList"))
        });
      }
    );
  };
  clickFavorite = () => {
    this.setState(
      {
        isFavorite: "favorite",
        clickedCharts: "",
        whichCategorylist: ""
      },
      () => this._loadFirebase()
    );
  };

  handleChange = e => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    });
    // switch 使用方法，將常用的放在最下面，可以提升效能
    switch (value) {
      case "video":
        this.onVideo(e, "video");
        break;
      case "live":
        this.setState(
          {
            keyword: "音樂"
          },
          () => this.onLive(e, "live")
        );
        break;
      case "popular":
        this.setState(
          {
            optionsOrderBind: "viewCount"
          },
          () => this.onVideo(e, "video")
        );
        break;
      case "date":
        this.setState(
          {
            optionsOrderBind: "date"
          },
          () => this.onVideo(e, "video")
        );
        break;
      case "all":
        this.setState(
          {
            optionsTimeBind: "1911-11-01T00:00:00Z"
          },
          () => this.onVideo(e, "video")
        );
        break;
      case "month":
        const currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - 30);
        const parseDate = currentDate.toJSON();
        this.setState(
          {
            optionsTimeBind: parseDate
          },
          () => this.onVideo(e, "video")
        );
        break;
      default:
        return false;
    }
  };
  handleSelect = event => {
    const { keyword, optionsTimeBind } = this.state;
    const target = event.target;
    const id = target.id;
    const value = target.value;

    this.setState({
      [id]: value
    });
  };

  onClear = e => {
    this.setState({
      keyword: ""
    });
  };
  onSearch = e => {
    e.preventDefault();
    const { keyword, optionsOrderBind, optionsTimeBind } = this.state;

    let data = {};
    switch (this.state.isType) {
      case "video":
        data = {
          // 時間
          publishedAfter: optionsTimeBind,
          publishedBefore: new Date().toJSON(),
          q: keyword, //twice
          part: "snippet",
          maxResults: 30,
          // 排序
          order: optionsOrderBind //date,viewCount,rating,relevance,videoCount
        };
        break;
      case "live":
        data = {
          eventType: "live",
          maxResults: 30,
          part: "snippet",
          q: keyword,
          type: "video",
          order: optionsOrderBind
        };
        break;
    }

    this.YouTube_search(data);
  };
  onFastSearch(e) {
    const { textContent } = e.target;
    let { keyword, optionsOrderBind, optionsTimeBind } = this.state;
    keyword = textContent;

    let data = {};
    switch (this.state.isType) {
      case "video":
        data = {
          // 時間
          publishedAfter: optionsTimeBind,
          publishedBefore: new Date().toJSON(),
          q: keyword, //twice
          part: "snippet",
          maxResults: 30,
          // 排序
          order: optionsOrderBind //date,viewCount,rating,relevance,videoCount
        };
        break;
      case "live":
        data = {
          eventType: "live",
          maxResults: 30,
          part: "snippet",
          q: keyword,
          type: "video",
          order: optionsOrderBind
        };
        break;
    }

    this.YouTube_search(data);
    this.setState({
      keyword: textContent
    });

    // singerInfo
    // keyword
    Object.keys(this.state.singer_info).map(info => {
      if (info === keyword) {
        this.singerInfo(this.state.singer_info[info]);
      } else {
        this.refs.singerInfo.innerHTML = "";
      }
    });
  }
  onVideo = (e, key) => {
    e.preventDefault();
    const { keyword, optionsOrderBind, optionsTimeBind } = this.state;
    const data = {
      // 時間
      publishedAfter: optionsTimeBind,
      publishedBefore: new Date().toJSON(),
      q: keyword, //twice
      part: "snippet",
      maxResults: 30,
      // 排序
      order: optionsOrderBind //date,viewCount,rating,relevance,videoCount
    };

    this.YouTube_search(data);
    this.setState({
      isType: key
    });
  };
  onLive = (e, key) => {
    e.preventDefault();
    const { keyword, optionsOrderBind } = this.state;
    const data = {
      eventType: "live",
      maxResults: 30,
      part: "snippet",
      q: keyword,
      type: "video",
      order: optionsOrderBind
    };
    this.YouTube_search(data);
    this.setState({
      isType: key
    });
  };
  onPlayVideoAll = e => {
    e.preventDefault();
    this.YouTubeIframeRef.current.cueVideoById(this.state.favoriteData[0].id);
    this.setState({
      whichPlay: "pause",
      isToggle: false,
      nowPlay: this.state.favoriteData[0].id
    });
  };
  onPlaySearchAllList = ev => {
    this.setState({
      isFavorite: "searchList"
    });
    localStorage.setItem("searchList", JSON.stringify(this.state.datas.items));
    this.tempList();
    ev.preventDefault();
  };
  removeFavorite = (e, item) => {
    var str = "";
    CoolRef.on("value", snapshot => {
      var data = snapshot.val();
      for (var i in data) {
        if (data[i].id === item.id) {
          CoolRef.child(i).remove();
        }
      }
    });

    // this._loadFirebase();
  };

  onFavorite = (e, item) => {
    // console.log(item);
    CoolRef.push(item);
    this._loadFirebase();
  };
  addToFavorite = (e, item) => {
    // console.log(item);
    CoolRef.push(item);
    this.setState({
      isFavorite: "followme"
    });
  };
  onMenuDownToggle = e => {
    this.favoriteListWrap.current.classList.toggle("active");
  };
  tabsClassBind = value => {
    if (this.state.isType === value) {
      return "nav-link btn-primary active";
    } else {
      return "nav-link";
    }
  };
  async tempList() {
    const datas = JSON.parse(localStorage.getItem("searchList"));
    this.setState({
      searchList: datas
    });
  }
  async kkTransYouTube(val, callback) {
    const data = {
      q: `${val.album.artist.name} ${val.name}`,
      part: "snippet",
      maxResults: 30,
      // 排序
      order: "relevance" //date,viewCount,rating,relevance,videoCount
    };
    let res = await _getYouTube("search", serialize(data));
    let songId = await res.items[0].id.videoId;
    const videosData = {
      part: "snippet,contentDetails,statistics",
      id: songId.toString()
    };
    let videos = await _getYouTube("videos", serialize(videosData));
    const videosResult = videos.items[0];
    return callback(videosResult);
  }
  onPlaykkboxToYou = async (e, val) => {
    e.preventDefault();
    const { nodeName } = e.target;
    if (nodeName === "BUTTON" || nodeName === "circle" || nodeName === "svg") {
      return false;
    }
    const youtuObject = await this.kkTransYouTube(val, res => res);
    this.onplayVideo(e, youtuObject);
    this.setState({
      nowPlay: val.id
    });
  };
  onplayVideo = (e, item) => {
    e.preventDefault();
    // const { nodeName } = e.target;
    // if (nodeName === "BUTTON" || nodeName === "circle" || nodeName === "svg") {return false}
    this.YouTubeIframeRef.current.cueVideoById(item.id);
    this.setState({
      nowPlay: item.id
    });
  };
  onPrev = (e, item) => {
    e.preventDefault();
    // 現在的播放清單 this.state.favoriteData
    // 現在播放的ID this.state.nowPlay
    const idx = this.state.favoriteData
      .map((el, i) => {
        return el.id;
      })
      .indexOf(this.state.nowPlay);
    if (idx === 0) return false;
    this.YouTubeIframeRef.current.cueVideoById(
      this.state.favoriteData[idx - 1].id
    );
    this.setState({
      nowPlay: this.state.favoriteData[idx - 1].id
    });
  };
  onPlay = (e, item) => {
    e.preventDefault();
    this.YouTubeIframeRef.current._onPlay();
    this.setState({
      whichPlay: "pause"
    });
  };
  onPause = (e, item) => {
    e.preventDefault();
    this.YouTubeIframeRef.current._onPause();
    this.setState({
      whichPlay: "play"
    });
  };
  onNext = (e, item) => {
    e.preventDefault();
    // 現在的播放清單 this.state.favoriteData
    // 現在播放的ID this.state.nowPlay
    const idx = this.state.favoriteData
      .map((el, i) => {
        return el.id;
      })
      .indexOf(this.state.nowPlay);
    if (idx === this.state.favoriteData.length) return false;
    this.YouTubeIframeRef.current.cueVideoById(
      this.state.favoriteData[idx + 1].id
    );
    this.setState({
      nowPlay: this.state.favoriteData[idx + 1].id
    });
  };

  handleNowPlay = val => {
    this.setState({
      nowPlay: val
    });
  };
  handleShow2 = () => {
    this.setState({ searchModal: true });
  };

  handleHide2 = () => {
    this.setState({ searchModal: false });
  };

  getNowPlay = data => {
    this.setState({
      nowPlay: data
    });
  };
  singerInfo(data) {
    this.refs.singerInfo.innerHTML = `
     <div className="col-12 col-sm-3">
                   <div className="card border-info flex-nowrap flex-row flex-sm-column mb-3">
                     <div className="card-header p-0">
                       <img
                         src=${data[0].img} className="img-fluid"
                       />
                     </div>
                     <div className="card-body text-info">
                       <p className="card-text">
                        ${data[0].descript}
                       </p>
                     </div>
                   </div>
                 </div>`;
  }
  render() {
    const { datas, favoriteData, kkboxChartList, isFavorite } = this.state;
    // var nowPlayList =
    //   favoriteData && favoriteData.filter(el => el.id === this.state.nowPlay);
    const nowPlayFn = () => {
      if (isFavorite === "kkboxList") {
        return (
          kkboxChartList &&
          kkboxChartList.filter(el => el.id === this.state.nowPlay)
        );
      } else if (isFavorite === "favorite" || isFavorite === "followme") {
        return (
          favoriteData &&
          favoriteData.filter(el => el.id === this.state.nowPlay)
        );
      } else {
        return false;
      }
    };

    return (
      <AppContext.Consumer>
        {({ youtubeInfoList }) => {
          return (
            <React.Fragment>
              <div className="YouTube-Platform">
                {this.state.searchModal && (
                  <ModalSearch
                    datas={this.state.datas}
                    handleHide2={this.handleHide2}
                    keyword={this.state.keyword}
                    handleChange={this.handleChange}
                    onSearch={this.onSearch}
                    optTypeCheck={this.state.optTypeCheck}
                    optTimeCheck={this.state.optTimeCheck}
                    optPopularCheck={this.state.optPopularCheck}
                    onPlaySearchAllList={this.onPlaySearchAllList}
                    onFavorite={this.onFavorite}
                    onplayVideo={this.onplayVideo}
                  />
                )}
                <div className="play-video">
                  <div className="play-card">
                    <YouTubeIframe
                      ref={this.YouTubeIframeRef}
                      favoriteData={this.state.favoriteData}
                      getNowPlay={this.getNowPlay}
                    />
                  </div>
                  <YoutubePanel
                    onPrev={this.onPrev}
                    onPlayVideoAll={this.onPlayVideoAll}
                    onNext={this.onNext}
                    onPause={this.onPause}
                    onPlay={this.onPlay}
                    nowPlay={nowPlayFn().length === 0 ? false : nowPlayFn()}
                    favoriteData={this.state.favoriteData}
                    isFavorite={this.state.isFavorite}
                    whichPlay={this.state.whichPlay}
                    CoolRef={CoolRef}
                  />
                </div>
                <div className="main">
                  <PlayCategory
                    handleShow2={this.handleShow2}
                    clickSearchList={this.clickSearchList}
                    clickFavorite={this.clickFavorite}
                    clickedCharts={this.state.clickedCharts}
                    clickCharts={this.clickCharts}
                    whichCategorylist={this.state.whichCategorylist}
                    kkboxCharts={this.state.kkboxCharts}
                    clickKkboxFollowFn={this.clickKkboxFollowFn}
                    changeCategoryList={() => {
                      this.setState({
                        whichCategorylist: "kkbox"
                      });
                    }}
                  />
                  <div className="play-hito">
                    <PlayList
                      kkboxChartList={this.state.kkboxChartList}
                      onplayVideo={this.onplayVideo}
                      addToFavorite={this.addToFavorite}
                      kkTransYouTube={this.kkTransYouTube}
                      onPlaykkboxToYou={this.onPlaykkboxToYou}
                      favoriteData={this.state.favoriteData}
                      isFavorite={this.state.isFavorite}
                      nowPlay={this.state.nowPlay}
                      onFavorite={this.onFavorite}
                      searchList={this.state.searchList}
                      removeFavorite={this.removeFavorite}
                      addToFavorite={this.addToFavorite}
                    />
                  </div>
                </div>
              </div>
            </React.Fragment>
          );
        }}
      </AppContext.Consumer>
    );
  }
}

export default YouTubeAPIBody;
