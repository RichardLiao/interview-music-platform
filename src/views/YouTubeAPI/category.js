import React from "react";

const PlayCategory = ({
  kkboxCharts,
  handleShow2,
  clickSearchList,
  clickFavorite,
  clickCharts,
  clickedCharts,
  whichCategorylist,
  clickKkboxFollowFn,
  changeCategoryList
}) => {
  return (
    <React.Fragment>
      <ul className="songsourcelist d-flex align-items-center pl-0">
        <li>
          <button className="btn btn-link" onClick={handleShow2}>
            <img
              src={require("../../static/img/search.svg")}
              style={{
                width: "26px",
                position: "relative",
                top: "2px"
              }}
            />
          </button>
        </li>
        {!!localStorage.getItem("searchList") && (
          <li>
            <button className="btn btn-link pl-0" onClick={clickSearchList}>
              <img
                draggable="false"
                className="emoji"
                alt="💓"
                src={require("../../static/img/file-text.svg")}
                style={{
                  width: "26px",
                  position: "relative",
                  top: "1px"
                }}
              />
            </button>
          </li>
        )}

        <li>
          <button className="btn btn-link pl-0" onClick={clickFavorite}>
            <img
              draggable="false"
              className="emoji"
              alt="💓"
              src="https://twemoji.maxcdn.com/2/72x72/1f493.png"
              style={{ width: "26px" }}
            />
          </button>
        </li>
        <li className="mr-2">
          <button
            className={`btn good-lable good-gradient-primary text-white ${"followme" ===
              clickedCharts && "active"}`}
            onClick={changeCategoryList}
          >
            KKBOX
          </button>
        </li>
      </ul>

      <div className="px-3">
        <ul
          className="list-unstyled row flex-nowrap mb-0 categorylist"
          style={{ overflowX: "auto" }}
        >
          {whichCategorylist === "kkbox" && (
            <React.Fragment>
              <li className="ml-2">
                <button
                  className={`btn good-lable good-lable-primary ${"followme" ===
                    clickedCharts && "active"}`}
                  onClick={clickKkboxFollowFn}
                >
                  一起聽
                </button>
              </li>
              {kkboxCharts &&
                kkboxCharts.map(chart => {
                  return (
                    <li className="ml-2" key={chart.id}>
                      <button
                        className={`btn good-lable good-lable-primary ${chart.id ===
                          clickedCharts && "active"}`}
                        onClick={e => clickCharts(e, chart.id)}
                      >
                        {chart.description}
                      </button>
                    </li>
                  );
                })}
            </React.Fragment>
          )}
        </ul>
      </div>
    </React.Fragment>
  );
};

export default PlayCategory;
