export default function(s) {
  s.props = {};
  s.onSetAppState = () => {};

  s.setup = function() {
    s.createCanvas(window.innerWidth, window.innerHeight / 2, s.WEBGL);
    // console.log("::: displayDensity:", s.displayDensity());
    // console.log("::: pixelDensity:", s.pixelDensity());
  };
  let angle = 0.0;
  s.draw = function() {
    s.background(51);
    s.directionalLight(255, 255, 255, 0, -1, 0);
    s.pointLight(255, 255, 255, -200, 0, 0); //在 x=-200 的座標上，打白光
    s.rectMode(s.CENTER);
    s.noStroke();
    s.rotateY(angle * 0.3);
    s.rotateZ(angle * 1.2);
    s.ambientMaterial(255, 255, 255);
    s.torus(100, 10);

    s.translate(s.mouseX, s.mouseY);
    angle += 0.069;
  };
}
