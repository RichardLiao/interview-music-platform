import pikachu from "../../static/img/pikachu1.jpg";
import * as p5 from "p5";
import "p5/lib/addons/p5.dom";
export default function(s) {
  s.props = {};
  s.onSetAppState = () => {};

  let cam;
  s.setup = function() {
    s.createCanvas(window.innerWidth, window.innerHeight / 2, s.WEBGL);

    cam = s.createCapture(p5.VIDEO);
    cam.size(300, 200);
    cam.hide()
  };
  let kitten;
  s.preload = function() {
    kitten = s.loadImage(pikachu);
  };
  let angle = 0.0;
  s.draw = function() {
    s.background(51);
    s.directionalLight(255, 255, 255, 0, -1, 0);
    s.pointLight(255, 255, 255, -200, 0, 0); //在 x=-200 的座標上，打白光
    s.rectMode(s.CENTER);
    s.noStroke();
    s.ambientMaterial(255, 255, 255);

    s.push()
    s.rotateY(angle * 0.3);
    s.rotateZ(angle * 1.2);
    s.texture(cam);
    s.box(100,100);
    s.pop()
    
    angle += 0.02;
    
  };
}
